/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useState } from 'react';
import type {Node} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  TextInput,
  Button,
  useColorScheme,
  View,
} from 'react-native';

import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

import ReactNativeBiometrics from 'react-native-biometrics';

import AsyncStorage from '@react-native-async-storage/async-storage';

const Section = ({children, title}): Node => {
  const isDarkMode = useColorScheme() === 'dark';
  return (
    <View style={styles.sectionContainer}>
      <Text
        style={[
          styles.sectionTitle,
          {
            color: isDarkMode ? Colors.white : Colors.black,
          },
        ]}>
        {title}
      </Text>
      <Text
        style={[
          styles.sectionDescription,
          {
            color: isDarkMode ? Colors.light : Colors.dark,
          },
        ]}>
        {children}
      </Text>
    </View>
  );
};

(async () => {
    const sensor = await ReactNativeBiometrics.isSensorAvailable();
     console.log(sensor)
        })();

const App: () => Node = () => {
  const isDarkMode = useColorScheme() === 'dark';

  const backgroundStyle = {
    backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
  };
    const [name, setName] = useState('');
    const [surname, setSurname] = useState('');
    const [registeredSurname, setRegisteredSurname] = useState('');
    const [registeredName, setRegisteredName] = useState('');

    getName = async () => {
        const registeredName = await AsyncStorage.getItem('name')
        return setRegisteredName(registeredName)
    }

    getSurname = async () => {
        const registeredSurname = await AsyncStorage.getItem('surname')
        return setRegisteredSurname(registeredSurname)
    }

    validateLogin = async () => {
            const keyExists = await ReactNativeBiometrics.biometricKeysExist()
            if (!keyExists) {
                return alert('no fingerprint registered')
            }
           ReactNativeBiometrics.simplePrompt({
               promptMessage: 'Vérification de l\'empreinte'
             })
             .then((resultObject) => {
               const { success } = resultObject
                if (success) {
                      getName()
                      getSurname()
                      if (registeredName === name && registeredSurname === surname) {
                        alert('logged in')
                      }
                } else {
                  return alert('401 : access denied')
                }
             })
    };

    validateRegister = async() => {
            if (!name || !surname) {
                alert('Please fill all fields');
            }
            else {
                try {
                   await AsyncStorage.setItem('name', name)
                   await AsyncStorage.setItem('surname', surname)
                   return alert('registered')
                } catch (e) {
                    return console.error(e)
                }
            }
        };

  return (
    <SafeAreaView style={backgroundStyle}>
      <StatusBar barStyle={isDarkMode ? 'light-content' : 'dark-content'} />
      <ScrollView
        contentInsetAdjustmentBehavior="automatic"
        style={backgroundStyle}>
        <View
          style={{
            backgroundColor: isDarkMode ? Colors.black : Colors.white,
          }}>
          <View>
            <Text style={[
                            styles.sectionTitle,
                            {
                              color: isDarkMode ? Colors.white : Colors.black,
                            },
                          ]}>
              Login
              </Text>
                <TextInput
                    style = {styles.input}
                    onChangeText={e => setName(e)}
                    value={registeredName}
                />
                <TextInput
                    style = {styles.input}
                    onChangeText={e => setSurname(e)}
                    value={registeredSurname}
                />
                <Button
                    title="Validate"
                    onPress={validateLogin}
                />
          </View>
          <View>
              <Text style={[
                              styles.sectionTitle,
                              {
                                color: isDarkMode ? Colors.white : Colors.black,
                              },
                            ]}>
                Register
                </Text>
              <TextInput
                  style = {styles.input}
                  placeholder={'type your name'}
                  onChangeText={e => setName(e)}
              />
              <TextInput
                  style = {styles.input}
                  placeholder={'type your surname'}
                  onChangeText={e => setSurname(e)}
              />
              <Button
                  title="Validate"
                  onPress={validateRegister}
              />
            </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
  input: {
      height: 40,
      margin: 12,
      borderWidth: 1,
      padding: 10,
    },
});

export default App;
